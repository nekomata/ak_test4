<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;

/**
 * Class AKOfficesOnYmaps
 */
class AKTest extends CBitrixComponent
{
    /**
     * @var float|int
     */
    private int $chacheTime = 60 * 60 * 24; //сутки
    /**
     * @var string
     */
    private string $pageTitle = 'Page Title';
    /**
     * @var array
     */
    private array $sectionsWithElements;
    /**
     * @var array
     */
    private array $sectionsTreeList;

    /**
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams = parent::onPrepareComponentParams($arParams);

        $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'] ?: $this->chacheTime;
        $arParams['PAGE_TITLE'] = $arParams['PAGE_TITLE'] ?: $this->pageTitle;

        return $arParams;
    }

    /**
     * @return mixed|void|null
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        if (!Loader::includeModule('iblock')) {
            return;
        }
        $this->getElements();
        $this->getSectionTree();
        ksort($this->sectionsTreeList);
        $this->arResult['sectionsTreeList'] = $this->sectionsTreeList;
        $this->includeComponentTemplate();
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function getElements()
    {
        if ($this->arParams['IBLOCK_ID']) {
            $this->arResult['elements'] = [];

            $iblock = Iblock::wakeUp($this->arParams['IBLOCK_ID']);
            $elements = $iblock->getEntityDataClass()::getList(
                [
                    'filter' => ['ACTIVE' => 'Y'],
                    'select' => ['ID', 'NAME', 'IBLOCK_SECTION_ID', 'P_TAGS'],
                    'cache' => [
                        'ttl' => $this->arParams['CACHE_TIME'],
                        'cache_joins' => true
                    ],
                ]
            )->fetchCollection();

            foreach ($elements as $element) {
                $sectionId = $element->getIblockSectionId();
                if (!isset($this->sectionsWithElements[$sectionId])) {
                    $this->sectionsWithElements[$sectionId] = [];
                }

                $elemData = [
                    'ID' => $element->getId(),
                    'NAME' => $element->getName(),
                    'SECTION_ID' => $sectionId,
                    'PROPS' => []
                ];

                $tags = [];
                foreach ($element->getPTags()->getAll() as $value) {
                    $tags[] = $value->getValue();
                }
                $elemData['PROPS']['TAGS'] = $tags;
                $this->sectionsWithElements[$sectionId][] = $elemData;
            }
        }
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function getSectionTree()
    {
        $sections = SectionTable::getList(
            [
                'order' => ['DEPTH_LEVEL' => 'DESC'],
                'filter' => [
                    'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                ],
                'select' => ['ID', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID'],
                'cache' => [
                    'ttl' => $this->arParams['CACHE_TIME'],
                    'cache_joins' => true
                ],
            ]
        )->fetchAll();

        foreach ($sections as $section) {
            $DL = (int)$section['DEPTH_LEVEL'];
            $parentDL = $DL - 1;
            if (isset($this->sectionsWithElements[$section['ID']])) {
                if (!isset($this->sectionsTreeList[$DL])) {
                    $this->sectionsTreeList[$DL] = [];
                }
                $this->sectionsTreeList[$DL][$section['ID']] = [
                    'ID' => (int)$section['ID'],
                    'NAME' => $section['NAME'],
                    'PARENT' => (int)$section['IBLOCK_SECTION_ID'],
                    'HAS_ELEMENTS' => 'Y',
                ];
                if ($parentDL) {
                    if (!isset($this->sectionsTreeList[$DL])) {
                        $this->sectionsTreeList[$parentDL] = [];
                    }
                    if (!isset($this->sectionsTreeList[$parentDL][$section['IBLOCK_SECTION_ID']])) {
                        $this->sectionsTreeList[$parentDL][$section['IBLOCK_SECTION_ID']] = [];
                    }
                }
            } elseif (isset($this->sectionsTreeList[$DL][$section['ID']])) {
                $this->sectionsTreeList[$DL][$section['ID']] = [
                    'ID' => (int)$section['ID'],
                    'NAME' => $section['NAME'],
                    'PARENT' => (int)$section['IBLOCK_SECTION_ID'],
                ];
                if ($parentDL) {
                    if (!isset($this->sectionsTreeList[$DL])) {
                        $this->sectionsTreeList[$parentDL] = [];
                    }
                    if (!isset($this->sectionsTreeList[$parentDL][$section['IBLOCK_SECTION_ID']])) {
                        $this->sectionsTreeList[$parentDL][$section['IBLOCK_SECTION_ID']] = [];
                    }
                }
            }
        }
    }

    /**
     * @param int $DL
     * @param array $section
     * @return string
     */
    public function printSection(int $DL, array $section): string
    {
        $html = '<div class="section" data-dl="' . $DL . '">';
        $html .= 'Раздел - ' . $section['NAME'];
        if ($section['HAS_ELEMENTS']) {
            $html .= $this->printElements($section['ID']);
        }
        if ($DL < count($this->sectionsTreeList)) {
            $nextDL = $DL + 1;
            foreach ($this->sectionsTreeList[$nextDL] as $children) {
                if ($children['PARENT'] == $section['ID']) {
                    $html .= $this->printSection($nextDL, $children);
                }
            }
        }
        $html .= '</div>';
        return $html;
    }

    /**
     * @param int $sectionId
     * @return string
     */
    private function printElements(int $sectionId): string
    {
        $html = '<div class="elements">';
        foreach ($this->sectionsWithElements[$sectionId] as $element) {
            $html .= '<div class="element">' . $element['NAME'] . '(' . implode(
                    ', ',
                    $element['PROPS']['TAGS']
                ) . ')</div>';
        }
        $html .= '</div>';
        return $html;
    }
}