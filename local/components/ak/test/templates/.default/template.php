<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$APPLICATION->SetTitle($arParams['PAGE_TITLE']);

?>
    <h1><?= $arParams['PAGE_TITLE'] ?></h1>
    <div class="container">
        <?php
        foreach ($arResult['sectionsTreeList'][1] as $section){
            echo $component->printSection(1,$section);
        }?>
    </div>

<?php
$APPLICATION->IncludeFile(SITE_TEMPLATE_INCLUDE_PATH . '/modals/main_page.php');
?>