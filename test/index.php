<?php

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->IncludeComponent(
    'ak:test',
    '',
    [
        'IBLOCK_ID' => '17',
        'PAGE_TITLE' => 'Страничка',
        'CACHE_TYPE' => 'A',
        'CACHE_TIME' => 600,
        'CACHE_NOTES' => '',
        'CACHE_GROUPS' => 'Y'
    ],
    false
);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');